# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby-rails-openredirect-checkredirect-to-taint"
  mode: taint
  type: sink
  pattern-sanitizers:
  - patterns:
    - pattern-either:
      - patterns:
        - pattern: |
            $F(...)
        - metavariable-pattern:
            metavariable: $F
            patterns:
            - pattern-not-regex: (params|url_for|cookies|request.env|permit|redirect_to)
      - pattern: |
          params.merge! :only_path => true
          ...
      - pattern: |
          params.slice(...)
          ...
      - pattern: |
          redirect_to [...]
      - patterns:
        - pattern: |
            $MODEL. ... .$M(...)
            ...
        - metavariable-regex:
            metavariable: $MODEL
            regex: '[A-Z]\w+'
        - metavariable-regex:
            metavariable: $M
            regex: (all|create|find|find_by|find_by_sql|first|last|new|from|group|having|joins|lock|order|reorder|select|where|take)
      - patterns:
        - pattern: |
            params.$UNSAFE_HASH.merge(...,:only_path => true,...)
            ...
        - metavariable-regex:
            metavariable: $UNSAFE_HASH
            regex: to_unsafe_h(ash)?
      - patterns:
        - pattern: params.permit(...,$X,...)
        - metavariable-pattern:
            metavariable: $X
            patterns:
            - pattern-not-regex: (host|port|(sub)?domain)
  pattern-sinks:
  - patterns:
    - pattern: $X
    - pattern-inside: |
        redirect_to $X, ...
    - pattern-not-regex: params\.\w+(?<!permit)\(.*?\)
  message: |
    The application was found handling redirect behavior with user-supplied input.  
    Do not pass `params` to `redirect_to` without the `:only_path => true` 
    hash value. Using `:only_path => true` ensures that the URL is interpreted 
    as a relative path, not allowing redirection to an arbitrary external URL, 
    thus mitigating the risk of open redirects. Alternatively, validate or 
    sanitize the input to ensure it's safe and intended.

    Secure Code Example:
    ```
    # Secure - Ensuring redirection is only to internal paths
    def secure_redirect
      redirect_to params[:redirect_url], only_path: true
    end
    ```
  languages:
  - "ruby"
  severity: "WARNING"
  metadata:
    attack-type: "openredirect"
    framework: "rails"
    kind: "code"
    shortDescription: "URL redirection to untrusted site 'open redirect'"
    category: "security"
    owasp:
    - "A1:2017-Injection"
    - "A03:2021-Injection"
    cwe: "CWE-601"
    security-severity: "LOW"
    references:
    - "https://cheatsheetseries.owasp.org/cheatsheets/Unvalidated_Redirects_and_Forwards_Cheat_Sheet.html"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/rails/security/brakeman/check-redirect-to.yaml"
