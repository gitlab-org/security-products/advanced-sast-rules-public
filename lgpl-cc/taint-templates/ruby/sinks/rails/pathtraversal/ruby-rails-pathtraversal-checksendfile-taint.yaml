# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby-rails-pathtraversal-checksendfile-taint"
  mode: taint
  type: sink
  pattern-sinks:
  - patterns:
    - pattern: |
        send_file ...
  pattern-sanitizers:
    - pattern: File.basename(...)
  message: |
    The application dynamically constructs file or path information with
    user input when calling the `send_file` method in Ruby. This practice
    can lead to a serious security vulnerability known as Local File
    Inclusion (LFI), allowing attackers to read arbitrary files on the server.
    LFI vulnerabilities can lead to the exposure of sensitive information, 
    such as configuration files, source code, and other data files, 
    which could be exploited to gain further access or to mount more 
    severe attacks.

    To mitigate this vulnerability, follow these steps:
    - Avoid direct user input: If possible, do not use user input 
    to determine file paths. Use a mapping of user input to server-side 
    defined file paths instead.
    - Use `File.basename`: If user input must be used to specify a file, 
    ensure that the input is normalized using `File.basename` to extract 
    the filename without any directory path. This prevents directory 
    traversal attacks.
    - Path allow listing: Implement an allow list of permitted files or 
    directories and check user input against this list before processing.
    
    Secure Code Example:
    ```
    def download
      filename = params[:filename]
      filepath = "path/to/secure/directory/#{File.basename(filename)}"

      # Optionally, further validate the resolved filepath against a whitelist

      if File.exist?(filepath) && !File.directory?(filepath)
        send_file filepath, x_sendfile: true
      else
        render plain: "File not found", status: :not_found
      end
    end
    ```
  languages:
  - "ruby"
  severity: "ERROR"
  metadata:
    attack-type: "pathtraversal"
    framework: "rails"
    kind: "code"
    owasp:
    - "A5:2017-Broken Access Control"
    - "A01:2021-Broken Access Control"
    cwe: "CWE-73"
    shortDescription: "External control of file name or path"    
    references:
    - "https://owasp.org/www-community/attacks/Path_Traversal"
    - "https://owasp.org/Top10/A01_2021-Broken_Access_Control/"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/rails/security/brakeman/check-send-file.yaml"
    category: "security"
    security-severity: "High"
    technology:
    - "ruby"
    - "rails"
