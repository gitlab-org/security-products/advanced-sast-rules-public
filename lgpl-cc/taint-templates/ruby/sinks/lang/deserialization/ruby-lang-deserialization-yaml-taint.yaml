# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
  - id: "ruby-lang-deserialization-yaml-taint"
    mode: taint
    type: sink
    pattern-sinks:
      - patterns:
          - pattern-either:
              - pattern: |
                  YAML.$LOAD($INPUT, ...)
          - pattern-not: |
              YAML.$LOAD(..., safe: true, ...)
          - pattern-not: |
              YAML.$LOAD("...", ...)
          - pattern-either:
              - pattern-inside: |
                  $FILE = File.read($INPUT, ...)
                  ...
              - pattern-inside: |
                  $FILENAME = $INPUT
                  ...
                  $FILE = File.read($FILENAME, ...)
                  ...
              - pattern: YAML.$LOAD(..., $FILE, ...)
          - metavariable-regex:
              metavariable: $LOAD
              regex: load(?:_stream)?\b|parse_stream
    message: |
      Unsafe deserialization from YAML. 
      YAML is a popular format for configuration files in Ruby applications 
      due to its readability and simplicity. However, the `load` method 
      provided by Ruby's YAML module can be dangerous when used to deserialize 
      data from untrusted sources. The method can instantiate objects based 
      on the YAML input, which can lead to remote code execution (RCE) if the 
      input contains malicious code. 
      
      To mitigate the risk of RCE through unsafe deserialization practices, 
      consider the following recommendations:
      - Prefer JSON for untrusted data: JSON is generally safer for 
      deserializing untrusted data because, by design, it does not support 
      the execution of arbitrary code. Ensure that any JSON deserialization 
      is performed with caution, validating and sanitizing the input before 
      use.
      - Use safe YAML loading methods: If you must use YAML, prefer safe 
      loading methods like `safe_load` instead of `load`. The `safe_load` method 
      limits the objects that can be deserialized, reducing the risk of 
      executing arbitrary code.
      - Static YAML files: Loading YAML from static, trusted files (like 
      configuration files that are not exposed to user input) is generally 
      safe. Ensure these files are securely managed and not modifiable by 
      untrusted sources.
      
      Secure Code Example:
      ```
      require 'yaml'
      require 'json'
      
      # Safe YAML deserialization
      def safe_load_yaml(file_path)
          YAML.safe_load(File.read(file_path), [Date, Time], [], true)
      end
      
      # Example of safely loading a static YAML file
      config_data = safe_load_yaml('config.yml')
      puts config_data.inspect
      
      # Safe JSON deserialization
      def safe_load_json(json_input)
          # Always validate and sanitize input before deserialization
          JSON.parse(json_input)
      end
      
      # Example JSON input
      json_input = '{"name": "Jack Monroe", "age": 30}'
      user_data = safe_load_json(json_input)
      puts user_data.inspect
      ```
    languages:
      - "ruby"
    severity: "ERROR"
    metadata:
      attack-type: "deserialization"
      framework: "rails"
      kind: "code"
      shortDescription: "Deserialization of untrusted data"
      category: "security"
      cwe: "CWE-502"
      owasp:
        - "A8:2017-Insecure Deserialization"
        - "A08:2021-Software and Data Integrity Failures"
      security-severity: "HIGH"