# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# source: https://github.com/semgrep/semgrep-rules/blob/release/python/flask/security/open-redirect.yaml
# yamllint enable
---
rules:
  - id: "python-flask-openredirect-taint"
    languages:
      - "python"
    mode: taint
    type: sink
    pattern-sinks:
      - patterns:
          - pattern-not-inside: |
              @$APP.route(...)
              def $X(...):
                ...
                if <... werkzeug.urls.url_parse($V) ...>:
                  ...
          - pattern: |
              flask.redirect($INPUT, ...)
          - focus-metavariable: $INPUT
          - pattern-not: "flask.redirect(flask.request.path)"
          - pattern-not: "flask.redirect(flask.request.path + ...)"
          - pattern-not: "flask.redirect(f\"{flask.request.path}...\")"
    message: |
      The use of `flask.redirect` with unvalidated user input can lead to 
      an Open Redirect vulnerability. This occurs when an application 
      redirects a user to an untrusted URL that is supplied as input, 
      potentially leading to phishing attacks or other malicious activity.

      Open Redirect vulnerabilities occur when an attacker can manipulate 
      the URL to which a user is redirected. This can be exploited to 
      redirect users to malicious sites, tricking them into providing 
      sensitive information or downloading malware. If user input is 
      passed directly to `flask.redirect` without validation, attackers 
      can supply a URL of their choosing, leading to an open redirect.

      To mitigate the vulnerability, take the following measures:
      - Validate and Sanitize Input: Always validate and sanitize the input 
        used in a redirect to ensure it points to a trusted domain. Only 
        allow redirects to known and safe URLs or use a whitelist 
        of allowed domains.
      - Use URL Generation Functions: Prefer using Flask's `url_for` function 
        to generate URLs within the application, which reduces the risk of 
        open redirects.
      - Avoid Direct User-Controlled Redirects: Avoid using user-controlled 
        data directly in `flask.redirect` without proper validation.

      Secure Code Example:
      ```
      from flask import Flask, redirect, request, url_for, abort

      app = Flask(__name__)

      @app.route('/redirect')
      def safe_redirect():
          next_url = request.args.get('next')
          
          # Mitigation: Validate the URL before redirecting
          if not next_url or not next_url.startswith('/'):
              return abort(400)  # Bad Request
          
          # Use url_for to safely generate the URL
          return redirect(url_for('index'))

      @app.route('/')
      def index():
          return "Welcome to the index page!"
      ```
      In this secure example, the `next_url` parameter is validated to 
      ensure it starts with a `/`, meaning it points to a relative path 
      within the application. This prevents redirection to an external, 
      potentially malicious site. The `url_for` function is also used to 
      generate the URL for redirection safely.
    metadata:
      attack-type: "openredirect"
      framework: "flask"
      kind: "code"            
      cwe: "CWE-601"
      shortDescription: "URL redirection to untrusted site ('Open Redirect')"
      owasp:
        - "A01:2021-Broken Access Control"
        - "A5:2017-Broken Access Control"
      category: "security"
      security-severity: "MEDIUM"
      references: |
        - https://flask-login.readthedocs.io/en/latest/#login-example
        - https://cheatsheetseries.owasp.org/cheatsheets/Unvalidated_Redirects_and_Forwards_Cheat_Sheet.html#dangerous-url-redirect-example-1
        - https://docs.python.org/3/library/urllib.parse.html#url-parsing
    severity: "WARNING"