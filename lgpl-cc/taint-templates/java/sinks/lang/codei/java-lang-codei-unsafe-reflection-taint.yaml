# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
  - id: java-lang-codei-unsafe-reflection-taint
    mode: taint
    type: sink
    languages:
      - java
    pattern-sinks:
      - patterns:
          - pattern: |
              Class.forName($CLASS,...)
          - focus-metavariable: $CLASS
    message: |
      The Class.forName() method is used to load a class dynamically at 
      runtime. When the class name passed to Class.forName() is not 
      hardcoded or is user-controlled, it introduces a potential security 
      risk. An attacker could exploit this to load arbitrary or malicious 
      classes, leading to unsafe code execution or reflection-based attacks.
      Allowing user input or unvalidated variables to determine which class 
      to load dynamically can lead to security issues such as:
        - Code Injection: Attackers can exploit this to inject malicious code, 
        especially if the class being loaded has unintended side effects or 
        exposes sensitive functionality.
        - Reflection Attacks: The dynamic class loading could be abused to 
        invoke private methods or constructors of system classes, bypassing 
        access controls.
        - Privilege Escalation: If an attacker can load a class that grants 
        higher privileges or performs unsafe operations, they could gain 
        unauthorized access to sensitive system resources.
      
      To mitigate this issue, avoid passing user-controlled or unvalidated 
      data to Class.forName(). Instead, ensure that the class names being 
      loaded are either hardcoded or validated against a whitelist of allowed 
      classes.
        - Whitelist Validation: Validate class names against a predefined list 
        of safe classes before loading them.
        - Hardcoding Classes: Where possible, avoid dynamic class loading altogether 
        and hardcode the class names that need to be loaded.

      Secure Code Example:
      ```
      import java.util.HashSet;
      import java.util.Set;

      public class SafeClassLoader {
          // Define a whitelist of allowed class names
          private static final Set<String> allowedClasses = new HashSet<>();

          static {
              allowedClasses.add("com.example.MyClass");
              allowedClasses.add("com.example.OtherClass");
          }

          public static Class<?> loadClassSafely(String className) throws ClassNotFoundException {
              // Validate the class name against the whitelist
              if (!allowedClasses.contains(className)) {
                  throw new ClassNotFoundException("Unauthorized class loading attempt: " + className);
              }
              // Safe class loading
              return Class.forName(className);
          }

          public static void main(String[] args) {
              try {
                  // Attempt to load a class safely
                  Class<?> clazz = loadClassSafely("com.example.MyClass");
                  System.out.println("Class loaded: " + clazz.getName());
              } catch (ClassNotFoundException e) {
                  e.printStackTrace();
              }
          }
      }
      ```
    metadata:
      attack-type: "codei" 
      framework: "lang"
      kind: "code"
      shortDescription: "Use of externally-controlled input to select classes or code ('Unsafe Reflection')"
      category: "security"
      cwe: "CWE-470"
      owasp:
        - "A1:2017-Injection"
        - "A03:2021-Injection"
      security-severity: "CRITICAL"      
    severity: "ERROR"
