# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "java-jdbi-sqli-handle-taint"
  languages:
  - "java"
  mode: taint
  type: sink
  message: |
    An unsaintized user input was used to construct an SQL query. This allows attackers to perform
    SQL Injection attacks.

    SQL Injection is a critical vulnerability that can lead to data or system compromise. By
    dynamically generating SQL query strings, user input may be able to influence the logic of
    the SQL statement. This could lead to an adversary accessing information they should
    not have access to, or in some circumstances, being able to execute OS functionality or code.

    Replace all dynamically generated SQL queries with parameterized queries. In situations where
    dynamic queries must be created, never use direct user input, but instead use a map or
    dictionary of valid values and resolve them using a user-supplied key.

    For example, some database drivers do not allow parameterized queries for `>` or `<` comparison
    operators. In these cases, do not use a user supplied `>` or `<` value, but rather have the
    user
    supply a `gt` or `lt` value. The alphabetical values are then used to look up the `>` and `<`
    values to be used in the construction of the dynamic query. The same goes for other queries
    where
    column or table names are required but cannot be parameterized.

    Example using `PreparedStatement` queries:
    ```
    // Some userInput
    String userInput = "someUserInput";
    // Your connection string
    String url = "...";
    // Get a connection from the DB via the DriverManager
    Connection conn = DriverManager.getConnection(url);
    // Create a prepared statement
    PreparedStatement st = conn.prepareStatement("SELECT name FROM table where name=?");
    // Set each parameters value by the index (starting from 1)
    st.setString(1, userInput);
    // Execute query and get the result set
    ResultSet rs = st.executeQuery();
    // Iterate over results
    while (rs.next()) {
        // Get result for this row at the provided column number (starting from 1)
        String result = rs.getString(1);
        // ...
    }
    // Close the ResultSet
    rs.close();
    // Close the PreparedStatement
    st.close();
    ```

    Example on using CriteriaBuilder to build queries 
    ```
    public List<YourEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<YourEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<YourEntity> root = query.from(YourEntity.class);

        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }
    ```

    For more information on SQL Injection see OWASP:
    https://cheatsheetseries.owasp.org/cheatsheets/SQL_Injection_Prevention_Cheat_Sheet.html
  pattern-propagators:
  - pattern: (StringBuffer $SB).append($SRC)
    from: $SRC
    to: $SB
  - pattern: (StringBuilder $SB).append($SRC)
    from: $SRC
    to: $SB
  pattern-sinks:
  - patterns:
    - pattern-either:
      - pattern: "(org.jdbi.v3.core.Handle $H).createQuery($ARG, ...)"
      - pattern: "(org.jdbi.v3.core.Handle $H).createScript($ARG, ...)"
      - pattern: "(org.jdbi.v3.core.Handle $H).createUpdate($ARG, ...)"
      - pattern: "(org.jdbi.v3.core.Handle $H).execute($ARG, ...)"
      - pattern: "(org.jdbi.v3.core.Handle $H).prepareBatch($ARG, ...)"
      - pattern: "(org.jdbi.v3.core.Handle $H).select($ARG, ...)"
      - pattern: "new org.jdbi.v3.core.statement.Script($H, $ARG)"
      - pattern: "new org.jdbi.v3.core.statement.Update($H, $ARG)"
      - pattern: "new org.jdbi.v3.core.statement.PreparedBatch($H, $ARG)"
    - focus-metavariable: $ARG
  metadata:
    attack-type: sqli
    framework: jdbi
    kind: code
    shortDescription: "Improper neutralization of special elements used in an SQL command ('SQL Injection')"
    category: "security"
    cwe: "CWE-89"
    technology:
    - "java"
    owasp:
    - "A1:2017-Injection"
    - "A03:2021-Injection"
    security-severity: "High"
  severity: "ERROR"
