# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# https://semgrep.dev/playground/r/ZRTQN09/java.lang.security.xmlinputfactory-external-entities-enabled.xmlinputfactory-external-entities-enabled
# yamllint enable
rules:
  - id: java-lang-xxe-xml-input-factory-atomic
    severity: "WARNING"
    languages:
      - "java"
    metadata:
      attack-type: "xxe"
      framework: "lang"
      kind: "code"    
      cwe: "CWE-611"
      shortDescription: "Improper restriction of XML external entity reference ('XXE')"
      owasp:
        - "A4:2017-XML External Entities (XXE)"
        - "A05:2021-Security Misconfiguration"
      security-severity: "MEDIUM"
      category: "security"
      license: "Commons Clause License Condition v1.0[LGPL-2.1-only]"

    message: |
      XML external entities are enabled for this XMLInputFactory. Enabling external 
      entities can make the application vulnerable to XML external entity attacks. 
      In an XXE attack, an attacker can exploit the processing of external entity 
      references within an XML document to access internal files, conduct 
      denial-of-service attacks, or SSRF (Server Side Request Forgery), potentially 
      leading to sensitive information disclosure or system compromise.
      
      To mitigate this vulnerability, disable external entities by
      setting "javax.xml.stream.isSupportingExternalEntities" to false.

      Secure Code Example:
      ```
      public GoodXMLInputFactory() {
        final XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        xmlInputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        xmlInputFactory.setProperty("javax.xml.stream.isSupportingExternalEntities", false);
      }
      ```
      For more details, refer to https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.md#xmlinputfactory-a-stax-parser

      references:
        - "https://semgrep.dev/blog/2022/xml-security-in-java"
        - "https://semgrep.dev/docs/cheat-sheets/java-xxe/"
        - "https://www.blackhat.com/docs/us-15/materials/us-15-Wang-FileCry-The-New-Age-Of-XXE-java-wp.pdf"
    pattern: |
      (javax.xml.stream.XMLInputFactory $XMLFACTORY).setProperty("javax.xml.stream.isSupportingExternalEntities",
      true);

