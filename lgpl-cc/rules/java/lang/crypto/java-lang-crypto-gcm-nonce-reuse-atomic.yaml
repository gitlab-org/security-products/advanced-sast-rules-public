# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: java-lang-crypto-gcm-nonce-reuse-atomic
  metadata:
    attack-type: "crypto"
    framework: "lang"
    kind: "code"
    shortDescription: "Reusing a nonce, key pair in encryption"
    category: "security"
    cwe: "CWE-323"
    technology:
    - "java"
    owasp:
    - "A3:2017-Sensitive Data Exposure"
    - "A02:2021-Cryptographic Failures"
    security-severity: "MEDIUM"
  languages:
  - java
  message: |
    GCM is a mode of operation for symmetric-key cryptographic block ciphers. GCM allows the usage of
    an initialization vector or nonce used to provide the initial state. The IV is an arbitrary number 
    that can be used just once in a cryptographic communication. Re use of initialization vectors 
    nullifies their usage, as the initial state of all GCMs with the same vector will effectively be
    the same.

    Instead of hard coding an initialization vector, it is recommended to use nextBytes() method from
    java.security.SecureRandom. This generates a user specified number of random bytes.

    Example using `java.security.SecureRandom`:
    ```
    Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
    SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");

    // Generate a new, random IV for each encryption operation
    SecureRandom secureRandom = new SecureRandom();
    // GCM standard recommends a 12-byte (96-bit) IV
    byte[] iv = new byte[12];
    secureRandom.nextBytes(iv);

    GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, iv);
    cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);
    ```

    For more information on Java Cryptography see:
    https://docs.oracle.com/en/java/javase/15/security/java-cryptography-architecture-jca-reference-guide.html
  patterns:
    - pattern-not-inside: |
        $RETURNTYPE $FUNC(...) {
          ...
          byte[] $Z = new byte[...];
          ...
        }
    - pattern-not-inside: |
        $RETURNTYPE $FUNC(...) {
          ...
          SecureRandom $SR = new SecureRandom();
          ...
          byte[] $IV = new byte[...];
          ...
          $SR.nextBytes($IV);
          ...
        }
    - pattern-either:
        - pattern: new GCMParameterSpec(...,$IVS, ...)
  severity: "WARNING"
