# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# https://github.com/semgrep/semgrep-rules/blob/release/java/rmi/security/server-dangerous-object-deserialization.yaml
# yamllint enable
---
rules:
  - id: java-lang-deserialization-server-dangerous-object-deserialization-atomic
    languages:
      - java
    patterns:
      - pattern-inside: |
          interface $INTERFACE extends java.rmi.Remote {
            ...
          }
      - pattern: |
          $RETURNTYPE $METHOD($PARAMTYPE $PARAM) throws java.rmi.RemoteException;
      - metavariable-pattern:
          metavariable: $PARAMTYPE
          language: generic
          patterns:
            - pattern-not: String
            - pattern-not: java.lang.String
            - pattern-not: boolean
            - pattern-not: Boolean
            - pattern-not: java.lang.Boolean
            - pattern-not: byte
            - pattern-not: Byte
            - pattern-not: java.lang.Byte
            - pattern-not: char
            - pattern-not: Character
            - pattern-not: java.lang.Character
            - pattern-not: double
            - pattern-not: Double
            - pattern-not: java.lang.Double
            - pattern-not: float
            - pattern-not: Float
            - pattern-not: java.lang.Float
            - pattern-not: int
            - pattern-not: Integer
            - pattern-not: java.lang.Integer
            - pattern-not: long
            - pattern-not: Long
            - pattern-not: java.lang.Long
            - pattern-not: short
            - pattern-not: Short
            - pattern-not: java.lang.Short
    severity: "WARNING"
    metadata:
      attack-type: "deserialization"
      framework: "lang"
      kind: "code"
      cwe: "CWE-502" 
      shortDescription: "Deserialization of untrusted data"
      owasp:
        - "A8:2017-Insecure Deserialization"
        - "A08:2021-Software and Data Integrity Failures"
      references:
        - "https://frohoff.github.io/appseccali-marshalling-pickles/"
        - "https://book.hacktricks.xyz/network-services-pentesting/1099-pentesting-java-rmi"
        - "https://youtu.be/t_aw1mDNhzI"
        - "https://github.com/qtc-de/remote-method-guesser"
        - "https://github.com/openjdk/jdk/blob/master/src/java.rmi/share/classes/sun/rmi/server/UnicastRef.java#L303C4-L331"
        - "https://mogwailabs.de/en/blog/2019/03/attacking-java-rmi-services-after-jep-290/"
      category: "security"
      security-severity: "Medium"
      license: "Commons Clause License Condition v1.0[LGPL-2.1-only]"
    message: |
      This application uses Java RMI (Remote Method Invocation) interfaces 
      that declare methods with parameters of arbitrary objects 
      ('$PARAMTYPE $PARAM') that could lead to insecure deserialization 
      vulnerabilities. Insecure deserialization occurs when an application 
      deserializes data from untrusted sources without proper sanitization, 
      potentially leading to arbitrary code execution, denial of service attacks, 
      and other critical vulnerabilities.

      Java RMI allows for remote communication between applications by 
      invoking methods on remote objects. When complex objects are used 
      as parameters in RMI method declarations, there is 
      a risk that a malicious actor could exploit the deserialization 
      process to execute arbitrary code on the server. This vulnerability 
      is especially significant if the parameter types are not among the safe, 
      immutable types like String, Integer, etc.

      To mitigate this vulnerability, use an integer ID to look up your 
      object, or consider alternative serialization schemes such as JSON.

      Secure Code Example:
      ```
      import java.rmi.Remote;
      import java.rmi.RemoteException;

      public interface SafeTicketService extends Remote {
        // Using String, which is a safe, immutable type
        boolean registerTicket(String ticketID) throws RemoteException;

        // Using primitive data types and wrappers, which are safe
        void visitTalk(int talkID) throws RemoteException;
        void poke(Integer attendeeID) throws RemoteException;
      }
      ```
