# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# https://github.com/semgrep/semgrep-rules/blob/release/java/spring/security/unrestricted-request-mapping.yaml
# yamllint enable
---
rules:
  - id: java-spring-csrf-unrestricted-requestmapping-atomic
    languages:
      - "java"
    patterns:
      - pattern-inside: |
          @org.springframework.web.bind.annotation.RequestMapping(...)
          $RETURNTYPE $METHOD(...) { ... }
      - pattern-not-inside: |
          @org.springframework.web.bind.annotation.RequestMapping(..., method = $X, ...)
          $RETURNTYPE $METHOD(...) { ... }
      - pattern: |
          org.springframework.web.bind.annotation.RequestMapping
    message: |
      Detected a method annotated with 'RequestMapping' that does not specify
      the HTTP method. CSRF protections are not enabled for GET, HEAD, TRACE, or
      OPTIONS, and by default all HTTP methods are allowed when the HTTP method
      is not explicitly specified. This means that a method that performs state
      changes could be vulnerable to CSRF attacks. Cross-Site Request Forgery (CSRF) 
      is a security vulnerability where an attacker tricks a user into performing 
      unintended actions on a web application where they are authenticated. This 
      can lead to unauthorized actions like changing user settings, transferring 
      funds, or altering passwords, all without the user's knowledge, by exploiting 
      the trust a web application has in the user's browser.
      
      To mitigate the issue, add the 'method' field and specify the HTTP method 
      (such as 'RequestMethod.POST').

      Secure Code Example:
      ```
      @RequestMapping(value = "/path", method = RequestMethod.POST)
      public void safe() {
          // State-changing operations performed within this method.
      }
      ```
    severity: "WARNING"
    metadata:
      attack-type: "csrf"
      framework: "spring"
      kind: "code"
      shortDescription: "Cross-site request forgery (CSRF)"
      category: "security"
      cwe: "CWE-352"
      owasp:
        - "A5:2017-Broken Access Control"
        - "A01:2021-Broken Access Control"
      security-severity: "MEDIUM"
      references:
        - "https://find-sec-bugs.github.io/bugs.htm#SPRING_CSRF_UNRESTRICTED_REQUEST_MAPPING"
      license: "Commons Clause License Condition v1.0[LGPL-2.1-only]"
