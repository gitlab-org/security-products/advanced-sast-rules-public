# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/ajinabraham/njsscan/blob/master/njsscan/rules/semantic_grep/traversal/express_hbs_lfr.yaml
# hash: e7a0a61
# yamllint enable
---
rules:
- id: javascript-node-pathtraversal-express-hbs-lfr-warning-taint
  mode: taint
  type: sink
  pattern-sinks:
  - patterns:
    - pattern-not-inside: |
        require('hbs')
        ...
    - pattern-inside: |
        require('express')
        ...
    - pattern-inside: function ... ($REQ, $RES, ...) {...}
    - pattern-inside: "$RES.render($VIEW, $INPUT)"
    - pattern-not-inside: "$RES.render($VIEW, { ... })"
    - pattern: $INPUT
  message: |
    This application is using untrusted user input in express render() function.
    If HBS Templating is used, it can lead to a Local File Read (LFR) vulnerability. 
    
    Passing untrusted user input as the 2nd argument to the `res.render()` 
    function in an Express.js application using the Handlebars (`hbs`) templating 
    engine can lead to a Local File Read (LFR) vulnerability due to the misuse 
    of the `layout` property in the context object. By setting the `layout` property 
    to a path like `../../../../etc/passwd`, an attacker can force the application 
    to render an unintended file as a layout, effectively reading arbitrary files on 
    the server. This vulnerability can expose sensitive files and data, leading to 
    information disclosure.
    
    An attacker can craft malicious input that traverses the filesystem and exposes 
    sensitive files. Consider sanitizing and validating all user input before passing it 
    to render() to prevent arbitrary file reads. Exclude Sensitive Properties - remove or 
    overwrite the `layout` property from the context object if it comes from user input.

    Sample safe use of express.render function
    ```
    app.get("/traversal/2", async (req, res) => {
        var indexPath = "index";
        res.render(indexPath, { title: "Index Page" })
    });
    ```
    For more details of this specific vulnerability, see: 
    https://blog.shoebpatel.com/2021/01/23/The-Secret-Parameter-LFR-and-Potential-RCE-in-NodeJS-Apps/
  languages:
  - "javascript"
  severity: "WARNING"
  metadata:
    attack-type: "pathtraversal"
    framework: "lang"
    kind: "code" 
    owasp: 
    - "A5:2017-Broken Access Control"
    - "A01:2021-Broken Access Control"
    cwe: "CWE-23" 
    shortDescription: "Relative path traversal"
    security-severity: "Low"
    category: "security"
    references:
      - "https://blog.shoebpatel.com/2021/01/23/The-Secret-Parameter-LFR-and-Potential-RCE-in-NodeJS-Apps/"
