# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/ajinabraham/njsscan/blob/master/njsscan/rules/semantic_grep/eval/eval_deserialize.yaml
# hash: e7a0a61
# yamllint enable
---
rules:
  - id: javascript-lang-serializetojs-deserialization-taint
    type: sink
    mode: taint
    pattern-sinks:
      - patterns:
          - pattern-either:
              - pattern-inside: |
                  $MOD = require('serialize-to-js')
                  ...
              - pattern-inside: |
                  import $MOD from 'serialize-to-js'
                  ...
          - pattern: |
              $MOD.deserialize(...)
      - pattern: |
          require('serialize-to-js').deserialize(...)
    message: |
      The application is using the `deserialize` method from the 
      `serialize-to-js` library with user input. This can lead to 
      unsafe deserialization vulnerabilities. Deserialization of 
      untrusted data can lead to security vulnerabilities, such 
      as remote code execution, data tampering, and other malicious 
      activities. When user input is deserialized without proper 
      validation and sanitization, it can allow attackers to exploit 
      the application by injecting malicious payloads.

      To mitigate the vulnerability, take the following measures:
      1. Avoid deserializing data from untrusted sources.
      2. Validate and sanitize user inputs before deserialization.
      3. Consider using safer serialization/deserialization libraries 
      that provide built-in security features.
      4. Implement strict schema validation to ensure only expected data 
      structures are deserialized.

      Secure Code Example:
      ```
      const allowedInputs = [
        '{"key1":"value1"}',
        '{"key2":"value2"}',
        '{"key3":"value3"}'
      ];

      // Function to check if the input matches any allowed pattern
      function isAllowedInput(input) {
        return allowedInputs.includes(input);
      }

      function secureFunction(userInput) {
          if (!isAllowedInput(userInput)) {
              throw new Error('Invalid input');
          }

          // Use a safer deserialization library
          const safeDeserializedData = JSON.parse(userInput);
          console.log(safeDeserializedData);
      }

      // Usage
      const userInput = '{"key2": "value2"}'; // Example of user-provided JSON input
      secureFunction(userInput);
      ```
    languages:
      - "javascript"
      - "typescript"
    severity: "ERROR"
    metadata:
      attack-type: "deserialization"
      framework: "serializetojs"
      kind: "code"
      owasp: 
        - "A8:2017-Insecure Deserialization"
        - "A08:2021-Software and Data Integrity Failures"
      cwe: "CWE-502"
      shortDescription: "Deserialization of Untrusted Data"
      security-severity: "CRITICAL"
      category: "security"   
