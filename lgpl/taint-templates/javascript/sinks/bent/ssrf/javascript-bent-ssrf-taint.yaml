# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/ajinabraham/njsscan/blob/master/njsscan/rules/semantic_grep/ssrf/ssrf_node.yaml
# hash: e7a0a61
# yamllint enable
---
rules:
- id: javascript-bent-ssrf-taint
  mode: taint
  type: sink
  pattern-sanitizers:
  - patterns:
    - pattern-either:
      - pattern: |
          if($VALIDATION){
          ...
          } 
      - pattern: |
          $A = $VALIDATION
          ...
          if($A){
          ...
          }
    - metavariable-pattern:
        metavariable: $VALIDATION
        pattern-either:
        - pattern: |
            $AL.includes(...)  
        - pattern: |
            $AL.indexOf(...) !== -1
        - pattern: |
            $AL.find(...) !== undefined
        - pattern: |
            $ALS.has(...)
  pattern-sinks:
  - pattern-either:
    - patterns:
      - pattern-either:
        - pattern-inside: |
            $BENT = require('bent');
            ...
        - pattern-inside: |
            import $BENT from 'bent';
            ...
      - pattern: $BENT(...)
    - patterns:
      - pattern-either:
        - pattern-inside: |
            $BENT = require('bent');
            ...
            $MTD = $BENT(...)
        - pattern-inside: |
            import $BENT from 'bent';
            ...
            $MTD = $BENT(...)
      - pattern: $MTD(...)
    - patterns:
      - pattern-either:
        - pattern-inside: |
            $BENT = require('bent');
            ...
            $GETBENT = $BENT('$PKG')
            ...
        - pattern-inside: |
            import $BENT from 'bent';
            ...
            $GETBENT = $BENT('$PKG')
            ...
      - pattern: $GETBENT(...)
      - metavariable-regex:
          metavariable: $PKG
          regex: ^(json|buffer)$
  message: |
      This application allows user-controlled URLs to be passed directly to HTTP client libraries. 
      This can result in Server-Side Request Forgery (SSRF).
      SSRF refers to an attack where the attacker can abuse functionality on 
      the server to force it to make requests to other internal systems within your 
      infrastructure that are not directly exposed to the internet. 
      This allows the attacker to access internal resources they do not have direct access to.
      
      Some risks of SSRF are:
  
      - Access and manipulation of internal databases, APIs, or administrative panels
      - Ability to scan internal network architecture and services
      - Can be used to pivot attacks into the internal network
      - Circumvent network segregation and firewall rules
  
      To avoid this, try using hardcoded HTTP request calls or a whitelisting object to 
      check whether the user input is trying to access allowed resources or not.
  
      Here is an example:
      ```
      var whitelist = [
        "https://example.com", 
        "https://example.com/sample"
      ]
  
      app.get('/ssrf/node-ssrf/axios/safe/3', function (req, res) {
        if(whitelist.includes(req.query.url)){
            let buffer = await getBuffer(req.query.url)
            res.send({
                obj,
                buffer
            })
        }
      });
      ``` 
      For more information on SSRF see OWASP:
      https://cheatsheetseries.owasp.org/cheatsheets/Server_Side_Request_Forgery_Prevention_Cheat_Sheet.html
  languages:
  - javascript
  severity: ERROR
  metadata:
    attack-type: ssrf
    framework: bent
    kind: code
    owasp:
    - A1:2017-Injection
    - A03:2021-Injection
    cwe: CWE-918
    shortDescription: Server-side request forgery (SSRF)
    security-severity: "High"
    category: security