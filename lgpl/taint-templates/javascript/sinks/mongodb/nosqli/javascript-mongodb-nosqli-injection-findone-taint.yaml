# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/ajinabraham/njsscan/blob/master/njsscan/rules/semantic_grep/database/nosql_find_injection.yaml
# hash: e7a0a61
# yamllint enable
---
rules:
- id: "javascript-mongodb-nosqli-injection-findone-taint"
  type: sink
  pattern-sanitizers:
    - patterns:
      - pattern-inside: |
          import $SANITIZE from 'mongo-sanitize'
          ...
      - pattern-either:
        - pattern: $SANITIZE(...)
        - pattern: |
            $OBJ.findOne({$KEY : <... $SANITIZE($REQ) ...>, ...}, ...)
        - pattern: |
            $QRY = {$KEY: <... $SANITIZE($REQ) ...>};
            ...
            $OBJ.findOne(<... $QRY ...>, ...)
        - pattern: |
            $QRY[$KEY] = <... $SANITIZE($REQ) ...>;
            ...
            $OBJ.findOne(<... $QRY ...>, ...)
  pattern-sinks:
    - patterns:
      - pattern-either:
        - pattern: |
            $OBJ.findOne({$KEY : <... $REQ ...>, ...}, ...)
        - pattern: |
            $QRY = {$KEY: <... $REQ ...>};
            ...
            $OBJ.findOne(<... $QRY ...>, ...)
        - pattern: |
            $QRY[$KEY] = <... $REQ ...>;
            ...
            $OBJ.findOne(<... $QRY ...>, ...)
  message: |
    There's a potential NoSQL Injection vulnerability in your JavaScript application that uses MongoDB. 
    The vulnerability arises when user-controlled data from sources like HTTP requests is passed directly 
    to the MongoDB `findOne` function.

    In the detected code pattern, user-controlled data is being used as input to the `findOne` function, 
    which allows executing arbitrary JavaScript code on the server. If this data contains malicious code or expressions, 
    an attacker could potentially execute arbitrary operations on the database, 
    leading to data manipulation, data exfiltration, or even complete system compromise.

    The impact of a NoSQL Injection vulnerability can be severe, including:
        - Unauthorized access to sensitive data stored in the database.
        - Modification or deletion of critical data.
        - Execution of arbitrary code on the server, leading to system compromise.
        - Potential for data exfiltration and privacy breaches.
    
    To mitigate the above issue, sanitize user input using libraries like `mongo-sanitize` and avoid passing user 
    input directly into the function.
    
    Secure code sample:
    ```
    app.post('/login', function (req, res) {
        // ruleid:rules_lgpl_javascript_database_rule-node-nosqli-injection
        x = sanitize(req.body.email)
        // ruleid:rules_lgpl_javascript_database_rule-node-nosqli-injection
        User.findOne({ 'email': x, 'password': "abc" }, function (err, data) {
            if (err) {
                res.send(err);
            } else if (data) {
                res.send('User Login Successful');
            } else {
                res.send('Wrong Username Password Combination');
            }
        })
    });
    ```
  languages:
  - "javascript"
  severity: "ERROR"
  metadata:
    attack-type: nosqli
    framework: mongodb
    kind: code
    owasp: 
    - "A1:2017-Injection"
    - "A03:2021-Injection"
    cwe: "CWE-943" 
    shortDescription: "Improper neutralization of special elements in data query logic"
    security-severity: "CRITICAL"
    category: "security"
