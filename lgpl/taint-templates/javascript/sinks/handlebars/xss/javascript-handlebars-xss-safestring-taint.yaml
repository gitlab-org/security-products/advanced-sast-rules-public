# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/ajinabraham/njsscan/blob/master/njsscan/rules/semantic_grep/xss/xss_templates.yaml
# hash: e7a0a61
# yamllint enable
---
rules:
- id: "javascript-handlebars-xss-safestring-taint"
  pattern-sinks:
  - pattern-either:
    - patterns:
      - pattern-inside: |
          $HB = require('handlebars')
          ...
      - pattern: "new $HB.SafeString(...)"
    - patterns:
      - pattern-inside: |
          import $HB from "handlebars";
          ...
      - pattern: "new $HB.SafeString(...)"
  pattern-sanitizers:
  - pattern-either:
    - patterns:
      - pattern-inside: |
          $HB = require('handlebars')
          ...
      - pattern: "$HB.escapeExpression(...)"
    - patterns:
      - pattern-inside: |
          import $HB from "handlebars";
          ...
      - pattern: "$HB.escapeExpression(...)"
  message: |
    This application is using a vulnerable method `Handlebars.SafeString(...)`.
    Handlebars SafeString method does not escape the data passed through it. 
    Untrusted user input passing through SafeString method can make the application 
    vulnerable to Cross-Site Scripting (XSS) attacks.
    
    XSS attacks are a type of security breach that occurs when an attacker manages to 
    inject malicious scripts into web pages viewed by other users. These scripts can then 
    execute in the context of the victim's session, allowing the attacker to bypass access 
    controls and potentially access sensitive information or perform actions on behalf of the    
    user. It is important to encode the data depending on the specific context it is used in. 

    Consider using the `Handlebars.escapeExpression` method to escape user input while 
    constructing SafeString to avoid potential security concerns. Following is a secure code 
    example.
    ```
      var returnObj = new Handlebars.SafeString("<h1>Handlebars safe string</h1>" +  Handlebars.escapeExpression(req.query.message))
      res.send(returnObj.string)
    ```
  languages:
  - "javascript"
  severity: "WARNING"
  type: sink
  metadata:
    attack-type: "xss"
    framework: "handlebars"
    kind: "code"
    owasp: 
    - "A7:2017-Cross-Site Scripting (XSS)"
    - "A03:2021-Injection"
    cwe: "CWE-79" 
    shortDescription: "Improper neutralization of input during web page generation (Cross-site Scripting)"
    security-severity: "MEDIUM"
    category: "security"
