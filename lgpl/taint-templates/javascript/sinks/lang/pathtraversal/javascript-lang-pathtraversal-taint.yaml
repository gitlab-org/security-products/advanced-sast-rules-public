# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/ajinabraham/njsscan/blob/master/njsscan/rules/semantic_grep/traversal/path_traversal.yaml
# hash: e7a0a61
# yamllint enable
---
rules:
  - id: javascript-lang-pathtraversal-taint
    mode: taint
    type: sink
    languages:
      - "javascript"
    pattern-sinks:
      - patterns:
          - pattern-either:
              - pattern-inside: |
                  $FS = require('fs')
                  ...
              - pattern-inside: |
                  import $FS from 'fs'
                  ...
              - pattern-inside: |
                  import { $FS } from 'fs'
                  ...
          - focus-metavariable: $REQ
          - pattern-either:
              - pattern: $FS.createReadStream($REQ)
              - pattern: $FS.readFile($REQ, ...)
              - pattern: $FS.readFileSync($REQ,...)
              - pattern: $FS.readFileAsync($REQ,...)
    message: |
      User input is being used directly in filesystem operations such 
      as `fs.createReadStream`, `fs.readFile`, `fs.readFileSync`, or 
      `fs.readFileAsync`. This can lead to security vulnerabilities 
      such as Path Traversal or Arbitrary File Access. When user input 
      is used directly in filesystem paths, an attacker can manipulate 
      the input to access unauthorized files or directories. This is 
      known as a Path Traversal attack. For example, an attacker can 
      input `../../../etc/passwd` to access sensitive system files.

      To mitigate the issue, validate and sanitize all user inputs before 
      using them in filesystem operations. Ensure that the input conforms 
      to the expected format and restrict it to specific allowed paths. 
      Using libraries for input validation and sanitization is highly 
      recommended.

      Secure Code Example:
      ```
      const express = require('express');
      const fs = require('fs');
      const path = require('path');
      const app = express();

      // Function to validate and sanitize user input
      function sanitizePath(input) {
        // Restrict input to a specific directory
        const basePath = path.join(__dirname, 'uploads');
        const sanitizedPath = path.normalize(path.join(basePath, input));

        // Ensure the resulting path is within the base path
        if (!sanitizedPath.startsWith(basePath)) {
          throw new Error('Invalid file path');
        }
        
        return sanitizedPath;
      }

      app.get('/readfile', function (req, res) {
        try {
          let userInput = req.query.file;
          let filePath = sanitizePath(userInput);

          fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
              res.status(500).send('File not found or error reading file');
            } else {
              res.send(data);
            }
          });
        } catch (err) {
          res.status(400).send('Invalid input');
        }
      });

      app.listen(3000, () => {
        console.log('Server is running on port 3000');
      });
      ```
      For more details see: 
      https://owasp.org/www-community/attacks/Path_Traversal
    severity: "ERROR"
    metadata:
      attack-type: "pathtraversal"
      framework: "lang"
      kind: "code"
      owasp: 
        - "A5:2017-Broken Access Control"
        - "A01:2021-Broken Access Control"
      cwe: "CWE-22"
      shortDescription: "Path traversal"
      security-severity: "High"
      category: "security"
