# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/ajinabraham/njsscan/blob/master/njsscan/rules/semantic_grep/xml/xxe_node.yaml
# hash: e7a0a61
# yamllint enable
---
rules:
- id: javascript-lang-xxe-libxmljs-taint
  mode: taint
  type: sink
  pattern-sinks:
  - patterns:
    - pattern-either:
      - pattern-inside: |
          import { ... } from 'libxmljs'
          ...
      - pattern-inside: |
          import $IMPORT from 'libxmljs'
          ...
      - pattern-inside: |
          require('libxmljs')
          ...
    - focus-metavariable: $INPUT
    - pattern-either:
      - pattern: |
          $LIBXML.parseXmlString(..., $INPUT, ...)
      - pattern: |
          $LIBXML.parseXml(..., $INPUT, ...)
      - pattern: |
          $PARSER = new libxmljs.SaxParser()
          ...
          $PARSER.parseString(..., $INPUT, ...)
      - pattern: |
          $PARSER = new libxmljs.SaxPushParser()
          ...
          $PARSER.push(..., $INPUT, ...)
  message: |
    The application is using the `libxmljs` library to parse XML 
    data directly from user input. This practice can lead to XML 
    External Entity (XXE) vulnerabilities if external entities 
    are not properly disabled. XXE vulnerabilities can allow attackers 
    to exploit the application by reading arbitrary files, performing 
    denial-of-service attacks, or even executing arbitrary code by 
    crafting malicious XML input. XXE vulnerabilities occur when the 
    XML parser processes external entities embedded within XML documents. 
    If these external entities are resolved, sensitive data can be 
    exposed or the application's functionality can be compromised.

    To prevent XXE vulnerabilities, configure the XML parser to disallow 
    external entity processing when handling XML data from untrusted sources. 
    Ensure thorough validation and sanitization of the input before parsing.

    Secure Code Example:
    ```
    const express = require('express');
    const app = express();
    const libxmljs = require('libxmljs');
    const bodyParser = require('body-parser');

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    app.post('/parse-xml', function (req, res) {
        const xmlInput = req.body.xml;

        // Validate the user input
        if (!isValidXmlInput(xmlInput)) {
            return res.status(400).send('Invalid input');
        }

        // Parse the XML securely
        try {
            const parser = new libxmljs.SaxParser();
            parser.on('startElement', function (name, attrs) {
                // Handle XML parsing logic
            });
            parser.on('error', function (err) {
                res.status(500).send('Error parsing XML');
            });

            // Ensure no external entities are processed
            parser.parseString(xmlInput, { noent: true, noblanks: true });

            res.send('XML parsed successfully');
        } catch (err) {
            res.status(500).send('Error parsing XML');
        }
    });

    function isValidXmlInput(input) {
        // Implement allowlist validation logic
        const allowedTags = ['<note>', '</note>', '<to>', '</to>', '<from>', '</from>', '<heading>', '</heading>', '<body>', '</body>'];
        for (const tag of allowedTags) {
            if (input.includes(tag)) {
                return true;
            }
        }
        return false;
    }
    app.listen(3000, function () {
        console.log('Server is running on port 3000');
    });
    ```
  languages:
  - "javascript"
  severity: "ERROR"
  metadata:
    attack-type: "xxe"
    framework: "lang"
    kind: "code"
    owasp: 
    - "A4:2017-XML External Entities (XXE)"
    - "A05:2021-Security Misconfiguration"
    cwe: "CWE-611" 
    shortDescription: "Improper restriction of XML external entity reference ('XXE')"
    security-severity: "High"
    category: "security"
