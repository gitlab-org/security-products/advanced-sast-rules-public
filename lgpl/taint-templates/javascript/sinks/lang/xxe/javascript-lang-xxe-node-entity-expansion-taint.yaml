# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/ajinabraham/njsscan/blob/master/njsscan/rules/semantic_grep/xml/xml_entity_expansion_dos.yaml
# hash: e7a0a61
# yamllint enable
---
rules:
  - id: "javascript-lang-xxe-node-entity-expansion-taint"
    mode: taint
    type: sink
    pattern-sinks:
      - patterns:
        - pattern-either:
          - pattern: |
              $PARSER = new expat.Parser(...)
              ...
              $PARSER.write(..., <... $SOURCE ...>, ...)
          - pattern: |
              $PARSER = new expat.Parser(...)
              ...
              $PARSER.parse(<... $SOURCE ...>,...)

    message: |
      rule detects a potential XML External Entity (XXE) vulnerability in your Node.js application 
      that uses the `expat` library for XML parsing. XXE vulnerabilities occur when 
      an XML parser processes user-supplied data that contains malicious external entity references. 
      These references can be used to disclose sensitive information, 
      launch Denial of Service (DoS) attacks, or even execute arbitrary code on the server.
      In the detected code pattern, user-controlled data from various sources 
      (such as request query, body, params, cookies, or headers) is being passed to the `write` method 
      of the `expat.Parser` object. If this data contains malicious external entity references, 
      it could lead to an XXE attack.

      The impact of an XXE vulnerability can be severe, including:
      - Disclosure of sensitive data: Attackers can access files on the server or internal network resources.
      - Denial of Service (DoS): Malicious entities can consume excessive system resources, leading to a DoS condition.
      - Remote Code Execution (RCE): In some cases, XXE vulnerabilities can be exploited to execute arbitrary code on the server.

    languages:
      - "javascript"
    severity: "ERROR"
    metadata:
      attack-type: xxe
      framework: lang
      kind: code
      owasp: 
      - "A4:2017-XML External Entities (XXE)"
      - "A05:2021-Security Misconfiguration"
      cwe: "CWE-776" 
      shortDescription: "Improper restriction of recursive entity references in DTDs (XML Entity Expansion)"
      security-severity: "CRITICAL"
      category: "security"