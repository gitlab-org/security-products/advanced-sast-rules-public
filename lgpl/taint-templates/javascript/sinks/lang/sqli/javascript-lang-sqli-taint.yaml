# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/ajinabraham/njsscan/blob/master/njsscan/rules/semantic_grep/database/sql_injection.yaml
# hash: e7a0a61
# yamllint enable
---
rules:
  - id: javascript-lang-sqli-taint
    mode: taint
    type: sink
    pattern-sinks:
      - patterns:
          - pattern-either:
              - pattern-inside: |
                  require('$LIB')
                  ...
              - pattern-inside: |
                  import $IMPORT from '$LIB'
                  ...
              - pattern-inside: |
                  import { ... } from '$LIB'
                  ...    
          - metavariable-regex:
              metavariable: $LIB
              regex: \b(sql-client|mysql|pg|mssql|oracledb|sequelize)\b
          - pattern: |
              $CON.query($INPUT, ...)
          - focus-metavariable: $INPUT
    message: |
      The application uses the `query` method from $LIB SQL library 
      without proper parameterization. This can lead to SQL injection 
      vulnerabilities, allowing attackers to execute arbitrary SQL 
      commands. SQL injection occurs when an attacker can manipulate 
      the SQL query by injecting malicious SQL code into the user input, 
      potentially gaining unauthorized access to the database.

      To mitigate the vulnerability, use parameterized queries or prepared 
      statements to separate code from data and prevent SQL injection attacks.
      By using parameterized queries, the input is treated as data rather 
      than executable code, preventing SQL injection attacks.

      Secure Code Example:
      ```
      // Import the necessary library
      const mysql = require('mysql');
      const connection = mysql.createConnection({ host: 'localhost', user: 'root', password: 'password', database: 'test' });

      const userInput = 'example';
      connection.query('SELECT * FROM users WHERE name = ?', [userInput], (error, results) => {
        if (error) throw error;
        console.log(results);
      });
      ```
    languages:
      - "javascript"
    severity: "ERROR"
    metadata:
      attack-type: "sqli"
      framework: "lang"
      kind: "code"      
      owasp: 
        - "A1:2017-Injection"
        - "A03:2021-Injection"
      cwe: "CWE-89" 
      shortDescription: "Improper neutralization of special elements used in an SQL command (SQL Injection)"
      security-severity: "High"
      category: "security"
