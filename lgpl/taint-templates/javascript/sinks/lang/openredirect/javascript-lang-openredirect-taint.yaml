# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/ajinabraham/njsscan/blob/master/njsscan/rules/semantic_grep/redirect/open_redirect.yaml
# hash: e7a0a61
# yamllint enable
---
rules:
  - id: javascript-lang-openredirect-taint
    mode: taint
    type: sink
    pattern-sinks:
      - pattern: $RES.redirect(...)
      - patterns:
          - pattern: $RES.$METHOD("$LOC", $VAR)
          - focus-metavariable: $VAR
          - metavariable-regex:
              metavariable: $METHOD
              regex: (header|set|append|setHeader)
          - metavariable-regex:
              metavariable: $LOC
              regex: (?i)^location$
      - patterns:
          - pattern: |-
              $RES.writeHead(..., { 
                          ..., 
                          location: $V,
                          ...
                      })
          - focus-metavariable: $V
      - pattern: $RES.location($VAR)
    pattern-sanitizers:
      - pattern-either:
          - patterns:
              - pattern-either:
                  - pattern: |-
                      if($VALIDATION){
                        ...
                      }
                  - pattern: |-
                      $A = $VALIDATION
                      ...
                      if($A){
                        ...
                      }
              - metavariable-pattern:
                  metavariable: $VALIDATION
                  pattern-either:
                    - pattern: $AL.includes(...)
                    - pattern: $AL.indexOf(...) !== -1
                    - pattern: $AL.find(...) !== undefined
                    - pattern: $ALS.has(...)
          - patterns:
              - pattern-either:
                  - pattern: $RES.redirect("$DOM" + ...)
                  - patterns:
                      - pattern-either:
                          - pattern: $RES.$METHOD("$LOC", "$DOM" + ...)
                          - pattern: |-
                              $V = "$DOM" + ...;
                              ...
                              $RES.$METHOD("$LOC", $V);
                      - metavariable-regex:
                          metavariable: $LOC
                          regex: (?i)^location$
                  - pattern: $RES.location("$DOM" + ...);
                  - pattern: |-
                      $V = "$DOM" + ...;
                      ...
                      $RES.location($V);
                  - patterns:
                      - pattern: |-
                          $RES.writeHead(..., { 
                                ..., 
                                location: "$DOM" + $V,
                                ...
                            })
                      - focus-metavariable: $V
                  - patterns:
                      - pattern: |-
                          $VAR = "$DOM" + $V
                          ...
                          $RES.writeHead(..., { 
                                ..., 
                                location: $VAR,
                                ...
                            })
                      - focus-metavariable: $V
              - metavariable-regex:
                  metavariable: $DOM
                  regex: (http(s)?:\/\/.*\/)
    message: |
      Passing untrusted user input in `redirect()` can result in an open redirect
      vulnerability. This could be abused by malicious actors to trick users into 
      being redirected to websites under their control to capture authentication
      information.  
      
      To mitigate the vulnerability, take the following measures:
      1. Always validate and sanitize user inputs, especially URL parameters
       or query strings that may influence the flow of the application.
      2. Use allowlists (lists of permitted URLs) to validate redirect targets 
       against known, trusted URLs before performing the redirect.
      3. Avoid directly using user input for redirecting. If unavoidable, ensure
       strict validation against an allowlist.

      Secure Code Example:
       ```
       // Define a list of explicitly allowed URLs for redirection
       const allowedUrls = [
           'https://www.example.com/page1',
           'https://www.example.com/page2',
           'https://secure.example.com/page3'
       ];

       app.get('/redirect/:url', (req, res) => {
           const url = decodeURIComponent(req.params.url);
           const isAllowed = allowedUrls.includes(url);
           if (isAllowed) {
               // If the URL is allowed, proceed with the redirect
               res.redirect(url);
           } else {
               res.status(400).send('Invalid redirect URL');
           }
       });
       ```
    languages:
      - javascript
    severity: WARNING
    metadata:
      attack-type: "openredirect"
      framework: "lang"
      kind: "code"
      shortDescription: "URL redirection to untrusted site 'open redirect'"
      category: "security"
      owasp:
        - "A1:2017-Injection"
        - "A03:2021-Injection"
      cwe: "CWE-601"
      security-severity: "Low"
