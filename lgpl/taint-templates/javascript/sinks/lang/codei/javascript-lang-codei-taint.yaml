# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/ajinabraham/njsscan/blob/master/njsscan/rules/semantic_grep/eval/eval_node.yaml
# hash: e7a0a61
# yamllint enable
---
rules:
  - id: javascript-lang-codei-taint
    mode: taint
    type: sink
    pattern-sinks:
      - pattern: |
          Function(...)
      - pattern: |
          new Function(...)
      - pattern: |
          setTimeout(...)
      - pattern: |
          setInterval(...)
    message: |
      The application is using `Function`, `setTimeout`, or `setInterval` 
      with user-controlled input, which can lead to code injection 
      vulnerabilities. Using these methods with user-controlled input can 
      allow an attacker to execute arbitrary code within the context of 
      the application, leading to severe security risks such as data theft, 
      data loss, or system compromise.

      To mitigate this, avoid using `Function`, `setTimeout`, and `setInterval` 
      with user input. If dynamic code execution is necessary, 
      ensure that the input is properly sanitized and validated, and perform
      validation against allowlist where possible.

      Secure Code Example:
      ```
        // Secure code
        const safeUserInput = "safe code"; // Example of safe user input
        const allowedInputs = ["safe code", "another safe code"]; // Allowlist of acceptable inputs

        if (allowedInputs.includes(safeUserInput)) {
            // Safe usage with validated input
            setTimeout(() => {
                eval(safeUserInput);
            }, 1000);
            setInterval(() => {
                eval(safeUserInput);
            }, 1000);
            const sayHello = new Function(
                  `return function (name) { var str = "Hello, ${name}"; return str; }`
              )();
            sayHello(safeUserInput);
        } else {
            throw new Error('Invalid input');
        }
      ```
    languages:
      - "javascript"
    severity: "WARNING"
    metadata:
      attack-type: "codei"
      framework: "lang"
      kind: "code"
      cwe: "CWE-94"
      shortDescription: "Improper control of generation of code ('Code Injection')"
      category: "security"
      owasp:
        - "A1:2017-Injection"
        - "A03:2021-Injection"
      security-severity: "MEDIUM"
