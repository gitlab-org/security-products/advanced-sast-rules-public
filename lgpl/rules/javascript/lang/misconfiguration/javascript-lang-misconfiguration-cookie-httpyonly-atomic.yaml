# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/ajinabraham/njsscan/blob/master/njsscan/rules/semantic_grep/headers/header_cookie.yaml
# hash: e7a0a61
# yamllint enable
---
rules:
- id: javascript-lang-misconfiguration-cookie-httpyonly-atomic
  patterns:
  - pattern-either:
    - pattern-inside: |
        $SESSION = require('cookie-session')
        ...
    - pattern-inside: |
        $SESSION = require('express-session')
        ...
  - pattern-either:
    - pattern-inside: |-
        $SESSION(<... {cookie:{httpOnly:false}} ...>,...)
    - pattern-inside: |
        $OPTS = <... {cookie:{httpOnly:false}} ...>;
        ...
        $SESSION($OPTS,...)
    - pattern-inside: |
        $OPTS = ...;
        ...
        $COOKIE = <... {httpOnly:false} ...>;
        ...
        $SESSION($OPTS,...)
    - pattern-inside: |
        $OPTS = ...;
        ...
        $OPTS.cookie = <... {httpOnly:false} ...>;
        ...
        $SESSION($OPTS,...)
    - pattern-inside: |
        $OPTS = ...;
        ...
        $COOKIE.httpOnly = false;
        ...
        $SESSION($OPTS,...)
    - pattern-inside: |
        $OPTS = ...;
        ...
        $OPTS.cookie.httpOnly = false;
        ...
        $SESSION($OPTS,...)
  message: |
    The `httpOnly` attribute is set to `false` for session cookies in 
    `cookie-session` or `express-session`. This can make the application 
    vulnerable to client-side script attacks (such as XSS), as cookies 
    can be accessed via JavaScript. When `httpOnly` is set to `false`, 
    cookies are accessible to JavaScript running in the browser, which 
    can lead to security vulnerabilities if an attacker manages to inject 
    malicious scripts into your application. Such scripts can steal cookies, 
    leading to session hijacking and other attacks.

    To mitigate this issue, always set the `httpOnly` attribute to `true` 
    for session cookies to ensure that they are not accessible via JavaScript. 
    This reduces the risk of client-side script attacks. 

    Secure Code Example:
    ```
      const session = require('express-session');
      
      app.use(session({
        secret: 'your_secret_key',
        resave: false,
        saveUninitialized: true,
        cookie: { httpOnly: true } // Ensure httpOnly is true
      }));

      // OR

      const cookieSession = require('cookie-session');
      
      app.use(cookieSession({
        name: 'session',
        keys: ['key1', 'key2'],
        cookie: { httpOnly: true } // Ensure httpOnly is true
      }));
    ```
  severity: "WARNING"
  languages:
  - "javascript"
  metadata:
    attack-type: "misconfiguration"
    framework: "lang"
    kind: "code"   
    owasp: 
    - "A6:2017-Security Misconfiguration"
    - "A05:2021-Security Misconfiguration"
    cwe: "CWE-1004"
    shortDescription: "Sensitive cookie without 'HttpOnly' flag"
    security-severity: "Low"
    category: "security"
