# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/ajinabraham/njsscan/blob/master/njsscan/rules/semantic_grep/database/sequelize_tls.yaml
# hash: e7a0a61
# yamllint enable
---
rules:
- id: javascript-lang-crypto-sequelize-tls-atomic
  message: |
    The application is using a Sequelize connection string that 
    does not specify TLS. Non-TLS connections are susceptible to 
    man-in-the-middle (MITM) attacks, where an attacker can 
    intercept and potentially alter the communication between the 
    application and the database server. Connecting to a database 
    without TLS encryption leaves sensitive data exposed during 
    transit. An attacker on the network can intercept the data, 
    leading to potential data breaches, data tampering, and 
    other security issues.

    To mitigate the issue, ensure that the database connection 
    uses TLS to encrypt the data in transit. Configure the 
    `dialectOptions` in Sequelize to enable SSL.

    Secure Code Example:
    ```
      const sequelize = new Sequelize({
        host: process.env.DB_HOST,
        database: process.env.DB_NAME,
        dialect: 'postgres',
        dialectOptions: {
          ssl: true
      });
    ```
  languages:
  - "javascript"
  severity: "WARNING"
  metadata:
    attack-type: "crypto"
    framework: "lang"
    kind: "code"
    owasp: 
    - "A3:2017-Sensitive Data Exposure"
    - "A02:2021-Cryptographic Failures"
    cwe: "CWE-319"
    shortDescription: "Cleartext transmission of sensitive information"
    security-severity: "Low"
    category: "security"
  patterns:
  - pattern: |
      {
        host: $HOST,
        database: $DATABASE,
        dialect: $DIALECT
       }
  - pattern-not: |
      {
        host: $HOST,
        database: $DATABASE,
        dialect: "postgres",
        dialectOptions: {
            ssl: true
        }
      }
  - pattern-not: |
      {
        host: $HOST,
        database: $DATABASE,
        dialect: $DIALECT,
        dialectOptions: {
          ssl: { ... }
        }
      }
  - metavariable-regex:
      metavariable: "$DIALECT"
      regex: "['\"](mariadb|mysql|postgres|oracle)['\"]"
